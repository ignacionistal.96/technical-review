Shipnow Technical Interview
===========================

This repository contains some of the technical exercises we use to evaluate and recruit the best talents for Shipnow.
If you casually bump into this repo, and you find the challenge appealing, please do not hesitate to contact us. 

Copyright (c) 2017 Shipnow SRL <developers@shipnow.com.ar>
https://shipnow.com.ar
http://docs.shipnow.apiary.io/